package com.aniket.productservice.command.api.commands;

import lombok.Builder;
import lombok.Data;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.math.BigDecimal;

@Data
@Builder
public class CreateProductCommand {
    //This is the Attribute for Data Aggregation
    @TargetAggregateIdentifier
    private String productId;
    private String name;
    private BigDecimal price;
    private String model;


}
