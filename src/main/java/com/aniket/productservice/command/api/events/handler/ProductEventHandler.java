package com.aniket.productservice.command.api.events.handler;

import com.aniket.productservice.commons.entity.ProductEntity;
import com.aniket.productservice.command.api.events.ProductCreatedEvent;
import com.aniket.productservice.commons.exception.GlobalExceptionEventPublisher;
import com.aniket.productservice.commons.exception.GlobalSystemExcption;
import com.aniket.productservice.commons.model.Product;
import com.aniket.productservice.commons.repository.ProductRepository;
import com.aniket.productservice.query.api.queries.GetProductQuery;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.config.ProcessingGroup;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.messaging.interceptors.ExceptionHandler;
import org.axonframework.queryhandling.QueryUpdateEmitter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.UUID;

@Component
@Slf4j
@RequiredArgsConstructor
@ProcessingGroup("product-error")
public class ProductEventHandler {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private GlobalExceptionEventPublisher globalExceptionEventPublisher;

    private final QueryUpdateEmitter queryUpdateEmitter;



    @EventHandler
    public void handleProductCreatedEvent(ProductCreatedEvent productCreatedEvent) {
        log.info(" EVENT HANDLER IS CALLED -------------- 5 ---- {}",productCreatedEvent);
      log.info("Whenever the Event get sourced to event Store - this Listener listen to it and store the data in DB ");
        ProductEntity product =ProductEntity.builder()
                .productId(productCreatedEvent.getProductId())
                .model(productCreatedEvent.getModel())
                .price(productCreatedEvent.getPrice())
                .name(productCreatedEvent.getName())
                .build();
        if(product.getPrice().compareTo(new BigDecimal(9999))==0)
        {
            //Here we are mimicing the Infra issue
            log.error("Price cannot be 9999");
            log.info("NOW EXCEPTION IS BEING FIRED FROM EVENT HANDLER --- 5--0");
            //Supose One exception happend - This need to be handled properly
             throw new RuntimeException("Product Not saved");
        }
        productRepository.save(product);
        log.info("Now Product saved and ready to be read");
        queryUpdateEmitter.emit(
                GetProductQuery.class,
                query -> true,
                new Product(product.getName(),product.getPrice(),product.getModel(),product.getProductId(),null));



    }

    @ExceptionHandler //Used from axon package
    public void handleEventHandlingException(ProductCreatedEvent productCreatedEvent, Exception ex) {
        log.info("LOCAL EVENT HANDLER EXCEPTION HANDLER CALLE4D ---- 5--1 ");
        log.error("Exception is : {}",ex.getMessage());
        globalExceptionEventPublisher.dispatchEvent(GlobalSystemExcption.builder()
                        .id(UUID.randomUUID().toString())
                        .cause(ex.getMessage())
                        .referenceEventData(productCreatedEvent.toString())
                .build());
        //Now need to decide what to do with this Exception - Ideally for the Infra issue we shoud throw exception and for Non infra issue we shoud fire error event
        // throw new RuntimeException(ex);
    }



}
