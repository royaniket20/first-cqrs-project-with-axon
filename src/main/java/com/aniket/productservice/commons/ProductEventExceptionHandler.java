package com.aniket.productservice.commons;

import lombok.extern.slf4j.Slf4j;
import org.axonframework.eventhandling.EventMessage;
import org.axonframework.eventhandling.EventMessageHandler;
import org.axonframework.eventhandling.ListenerInvocationErrorHandler;

@Slf4j
public class ProductEventExceptionHandler implements ListenerInvocationErrorHandler {
    @Override
    public void onError(Exception e, EventMessage<?> eventMessage, EventMessageHandler eventMessageHandler)  {
      log.info("LISTNER INVOCATION ERROR HANDLER CALLED ---- 5--2 ");
      log.info("Error Message : {}",eventMessage);
       log.info("Event Message Handler : {}" , eventMessageHandler);


/**
 * You can choose to retry, ignore or rethrow the exception. The exception will bubble up to the Event Processor level when rethrown.
 */
        throw new RuntimeException(e);
    }
}
