package com.aniket.productservice.commons;

import com.aniket.productservice.command.api.events.ProductCreatedEvent;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.eventhandling.*;

@Slf4j
public class ProductEventProcessingGroupExceptionHandler implements ErrorHandler {



    @Override
    public void handleError(ErrorContext errorContext)  {

        log.info("EVENT PROCESSOR GROUP ERROR HANDLER CALLED ---- 5-3 ");
        log.info("Error  errorContext: {}",errorContext);
        log.info("Event Processor : {}",errorContext.eventProcessor());
        log.info("Filed Events : {}",errorContext.failedEvents());
        ProductCreatedEvent productCreatedEvent = (ProductCreatedEvent)errorContext.failedEvents().get(0).getPayload();
        log.info("Fixing the Payload ---- {}",productCreatedEvent);
        //Now the @ErrorHandler of this Event Processor Will KickIn
       // throw  new RuntimeException(errorContext.error());
    }
}
