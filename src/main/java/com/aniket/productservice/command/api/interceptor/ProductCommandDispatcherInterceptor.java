package com.aniket.productservice.command.api.interceptor;

import com.aniket.productservice.command.api.commands.CreateProductCommand;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandMessage;
import org.axonframework.messaging.MessageDispatchInterceptor;

import java.util.List;
import java.util.function.BiFunction;

@Slf4j
public class ProductCommandDispatcherInterceptor implements MessageDispatchInterceptor<CommandMessage<?>> {
    @Override
    public BiFunction<Integer, CommandMessage<?>, CommandMessage<?>> handle(List<? extends CommandMessage<?>> list) {
        return (i, m) -> {
            if (CreateProductCommand.class.equals(m.getPayloadType())) {
                final CreateProductCommand createProductCommand = (CreateProductCommand) m.getPayload();
               if(createProductCommand.getName().equals("NULL")){
                    throw new IllegalStateException("Name is Null Which Is Not allowed");
                }
            }
            log.info("COMMAND DISPATCH INTERCEPTOR CALLED -----2 !!");
            return m;
        };
    }
}
