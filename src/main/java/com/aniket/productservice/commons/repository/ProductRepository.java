package com.aniket.productservice.commons.repository;

import com.aniket.productservice.commons.entity.ProductEntity;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<ProductEntity , String> {
}
