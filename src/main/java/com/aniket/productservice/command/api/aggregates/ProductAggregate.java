package com.aniket.productservice.command.api.aggregates;

import com.aniket.productservice.command.api.commands.CreateProductCommand;
import com.aniket.productservice.command.api.events.ProductCreatedEvent;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.messaging.interceptors.ExceptionHandler;
import org.axonframework.modelling.command.AggregateCreationPolicy;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.modelling.command.CreationPolicy;
import org.axonframework.spring.stereotype.Aggregate;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;

@Aggregate
@Slf4j
@NoArgsConstructor
public class ProductAggregate {

    private String name;
    private BigDecimal price;
    private String model;
    //This is the Attribute for Data Aggregation
    @AggregateIdentifier
    private String productId;

    @CommandHandler
    @CreationPolicy(AggregateCreationPolicy.ALWAYS)
    public void handle(CreateProductCommand  createProductCommand) {
          log.info(" COMMAND HANDLER IS CALLED ---------------4");
          log.info("Here we do Business Validation of Payload {}",createProductCommand);
          //Creating event that to be published to Event Store
        if(createProductCommand.getModel().equals("NULL")){
            throw new RuntimeException("Model Cannot be Null");
        }
        ProductCreatedEvent productCreatedEvent = new ProductCreatedEvent();
        BeanUtils.copyProperties(createProductCommand,productCreatedEvent);
        AggregateLifecycle.apply(productCreatedEvent);
    }

    //Now we need to have a handler to Update Aggregate whenever Insert/Update/Delete Happens
 //It is Only required if you want to Use the Aggregate State to do some Business Validation
    @EventSourcingHandler
    public void onProductCreate(ProductCreatedEvent productCreatedEvent){
        log.info("****** Event Sourcing Handler get called*****");
            this.productId=productCreatedEvent.getProductId();
             this.model=productCreatedEvent.getModel();
             this.name=productCreatedEvent.getName();
             this.price=productCreatedEvent.getPrice();

    }

    @ExceptionHandler //Used from axon package
    public void handleCommandHandlingException(Exception exception) {
        log.info("Exception get Caught in the Command Handling Layer - Dropping the Command");
        log.error("Exception is : {}",exception.getMessage());
        //Now need to decide what to do with this Excpetion
         throw new RuntimeException(exception);
    }


}
