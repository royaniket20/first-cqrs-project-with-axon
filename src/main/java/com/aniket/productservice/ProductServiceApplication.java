package com.aniket.productservice;

import com.aniket.productservice.command.api.interceptor.ProductCommandDispatcherInterceptor;
import com.aniket.productservice.command.api.interceptor.ProductCommandHandlerInterceptor;
import com.aniket.productservice.commons.ProductEventExceptionHandler;
import com.aniket.productservice.commons.ProductEventProcessingGroupExceptionHandler;
import org.axonframework.commandhandling.CommandBus;
import org.axonframework.commandhandling.SimpleCommandBus;
import org.axonframework.config.Configuration;
import org.axonframework.config.EventProcessingConfigurer;
import org.axonframework.eventhandling.ErrorHandler;
import org.axonframework.eventhandling.ListenerInvocationErrorHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.function.Function;

@SpringBootApplication
public class ProductServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductServiceApplication.class, args);
    }


    @Autowired
    public void configureProcessingGroupErrorHandling(EventProcessingConfigurer eventProcessingConfigurer){
        Function<Configuration, ListenerInvocationErrorHandler> listenerInvocationErrorHandlerFunction = (config)->new ProductEventExceptionHandler();
        // To configure a default ...
       // eventProcessingConfigurer.registerDefaultListenerInvocationErrorHandler(conf -> /* create listener error handler */)
        // ... or for a specific processing group:
        eventProcessingConfigurer.registerListenerInvocationErrorHandler("product-error",listenerInvocationErrorHandlerFunction);

        Function<Configuration, ErrorHandler> configurationErrorHandlerFunction = (config)->new ProductEventProcessingGroupExceptionHandler();

        // To configure a default ...
       // eventProcessingConfigurer.registerDefaultErrorHandler(conf -> /* create error handler */)
                // ... or for a specific processor:
        eventProcessingConfigurer.registerErrorHandler("product-error", configurationErrorHandlerFunction);

    }
    @Bean
    public CommandBus configureCommandBus() {
        CommandBus commandBus = SimpleCommandBus.builder().build();

        commandBus.registerDispatchInterceptor(new ProductCommandDispatcherInterceptor());

        commandBus.registerHandlerInterceptor(new ProductCommandHandlerInterceptor());
        return commandBus;
    }

}
