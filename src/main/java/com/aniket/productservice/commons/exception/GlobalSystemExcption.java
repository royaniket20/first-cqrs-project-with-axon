package com.aniket.productservice.commons.exception;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GlobalSystemExcption {
    String cause;
    String referenceEventData;
    String id;
}
