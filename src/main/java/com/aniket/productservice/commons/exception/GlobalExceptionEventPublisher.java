package com.aniket.productservice.commons.exception;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.eventhandling.gateway.EventGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
public class GlobalExceptionEventPublisher {

    @Autowired
    private EventGateway eventGateway;

    public void dispatchEvent(GlobalSystemExcption globalSystemExcption ) {
        log.info("Publishing Event for the Exception to Store in Error Log DB");

        eventGateway.publish(globalSystemExcption);
    }
}
