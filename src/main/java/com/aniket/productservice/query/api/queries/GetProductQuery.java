package com.aniket.productservice.query.api.queries;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GetProductQuery {

    private String productIdForFetch;
}
