package com.aniket.productservice.commons.exception;

import com.aniket.productservice.command.api.events.ProductCreatedEvent;
import com.aniket.productservice.commons.entity.ProductEntity;
import com.aniket.productservice.commons.model.Product;
import com.aniket.productservice.query.api.queries.GetProductQuery;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.queryhandling.QueryUpdateEmitter;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Slf4j
@Component
@RequiredArgsConstructor
public class GlobalSystemExceptionHandler {


    private final QueryUpdateEmitter queryUpdateEmitter;



    @EventHandler
    public void handleProductCreatedEvent(GlobalSystemExcption globalSystemExcption) {
        log.info("MANUALLY CALLED EXCEPTION EVENT CALLED {} -------6 ",globalSystemExcption);
        queryUpdateEmitter.emit(
                GetProductQuery.class,
                query -> true,
                new Product(null,null,null,null,globalSystemExcption.getCause()));



    }
}
