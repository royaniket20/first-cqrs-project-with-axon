package com.aniket.productservice.query.api.projections;

import com.aniket.productservice.command.api.events.ProductCreatedEvent;
import com.aniket.productservice.commons.entity.ProductEntity;
import com.aniket.productservice.commons.model.Product;
import com.aniket.productservice.commons.repository.ProductRepository;
import com.aniket.productservice.query.api.queries.GetProductQuery;
import com.google.common.collect.Lists;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.queryhandling.QueryHandler;
import org.axonframework.queryhandling.QueryUpdateEmitter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@Slf4j
@RequiredArgsConstructor
public class ProductProjection {

    @Autowired
    private ProductRepository productRepository;


    @QueryHandler
    public List<Product> getAllProducts (GetProductQuery getProductQuery){
      log.info("Here GetProductQuery can be used for passing Get parameters : {}",getProductQuery);
       return  Lists.newArrayList(productRepository.findAll()).stream().map(item->
            new Product(item.getName(),item.getPrice(),item.getModel(),item.getProductId(),null)).collect(Collectors.toList());
    }

    @QueryHandler
    public Product getProductById (GetProductQuery getProductQuery){
        log.info("Here GetProductQuery can be used for passing Get parameters : {}",getProductQuery);
        Optional<ProductEntity> productEntity = productRepository.findById(getProductQuery.getProductIdForFetch());
        if(productEntity.isPresent()){
            ProductEntity productEntityFinal = productEntity.get();
            return new Product(productEntityFinal.getName(),productEntityFinal.getPrice(),productEntityFinal.getModel(),productEntityFinal.getProductId(),null);
        }else{
            throw new RuntimeException("Product Not Found with Id : "+getProductQuery.getProductIdForFetch());
        }
    }

}
