package com.aniket.productservice.command.api.interceptor;

import com.aniket.productservice.command.api.commands.CreateProductCommand;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandMessage;
import org.axonframework.messaging.InterceptorChain;
import org.axonframework.messaging.MessageHandlerInterceptor;
import org.axonframework.messaging.unitofwork.UnitOfWork;

import java.math.BigDecimal;


@Slf4j
public class ProductCommandHandlerInterceptor implements MessageHandlerInterceptor<CommandMessage<?>> {



    @Override
    public Object handle(UnitOfWork<? extends CommandMessage<?>> unitOfWork, InterceptorChain interceptorChain) throws Exception {
            CommandMessage<?> command = unitOfWork.getMessage();
            log.info("*****Command Handler InterceptorChain being called-----3 -- START");
            if (CreateProductCommand.class.equals(command.getPayloadType())) {
                final CreateProductCommand createProductCommand = (CreateProductCommand) command.getPayload();
                if (createProductCommand.getPrice().compareTo(new BigDecimal(0)) == 0) {
                    log.info("Command Handler Execution Prevented --------- 3 --- END");
                    throw new RuntimeException("Price cannot be Zero !!!!");
                }else if (createProductCommand.getPrice().compareTo(new BigDecimal(-99)) == 0) {
                    createProductCommand.setPrice(new BigDecimal(99));
                    Object proceed = interceptorChain.proceed();
                    log.info("Command Handler Execution Done --------- 3 --- END{}",proceed);
                    return proceed;
                } else {
                    Object proceed = interceptorChain.proceed();
                    log.info("Command Handler Execution Done --------- 3 --- END{}",proceed);
                    return proceed;
                }
            }

        return null;
    }

}
