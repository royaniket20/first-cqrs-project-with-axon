package com.aniket.productservice.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class Product {
    private String name;
    private BigDecimal price;
    private String model;
    private String productId;
    private String errorMessage;

}
