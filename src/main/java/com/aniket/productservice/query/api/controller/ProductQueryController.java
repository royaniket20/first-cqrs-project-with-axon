package com.aniket.productservice.query.api.controller;

import com.aniket.productservice.commons.model.Product;
import com.aniket.productservice.query.api.queries.GetProductQuery;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.axonframework.queryhandling.SubscriptionQueryResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/quiries/products")
@Slf4j
public class ProductQueryController {

    //Axon Provide this
    @Autowired
    private QueryGateway queryGateway;

    @GetMapping
    public List<Product> getAllProducts() {
        GetProductQuery getProductQuery = GetProductQuery.builder().build();
        CompletableFuture<List<Product>> listCompletableFuture = queryGateway.query(getProductQuery, ResponseTypes.multipleInstancesOf(Product.class));
        return listCompletableFuture.join();
    }

    @GetMapping("/{productId}")
    public Product getProductById(@PathVariable String productId) {
        GetProductQuery getProductQuery = GetProductQuery.builder()
                .productIdForFetch(productId)
                .build();
        CompletableFuture<Product> completableFuture = queryGateway.query(getProductQuery, ResponseTypes.instanceOf(Product.class));
        return completableFuture.join();
    }

    @GetMapping(value = "/reactive" , produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Flux<Product> getAllProductsReactive() {
        GetProductQuery getProductQuery = GetProductQuery.builder().build();
        SubscriptionQueryResult<List<Product>,Product> subscriptionQueryResult = queryGateway
                .subscriptionQuery(getProductQuery,
                        ResponseTypes.multipleInstancesOf(Product.class),ResponseTypes.instanceOf(Product.class));
        Mono<List<Product>> initialResults = subscriptionQueryResult.initialResult();
        Flux<Product> initials =  initialResults
                .flatMapIterable(item->item)/*.delayElements(Duration.ofMillis(100))
                .log()*/;
        Flux<Product> updates = subscriptionQueryResult.updates()/*.delayElements(Duration.ofMillis(100)).log()*/;
        //Now We will merge the Flux
        Flux<Product> merged = Flux.concat(initials,updates).log("IDENTIFIER");
       return merged;
    }

}
