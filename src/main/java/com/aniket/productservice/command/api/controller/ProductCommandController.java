package com.aniket.productservice.command.api.controller;

import com.aniket.productservice.command.api.commands.CreateProductCommand;
import com.aniket.productservice.commons.exception.GlobalExceptionEventPublisher;
import com.aniket.productservice.commons.exception.GlobalSystemExcption;
import com.aniket.productservice.commons.model.Product;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandCallback;
import org.axonframework.commandhandling.CommandMessage;
import org.axonframework.commandhandling.CommandResultMessage;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.UUID;

@RestController
@RequestMapping("/commands/products")
@Slf4j
public class ProductCommandController {

    //Axon Provide this
    @Autowired
    private CommandGateway commandGateway;

    @Autowired
    private GlobalExceptionEventPublisher globalExceptionEventPublisher;
    private CommandCallback<CreateProductCommand, Object> createProductCommandObjectCommandCallback;

    @PostMapping
    public String addProduct(@RequestBody Product product) throws UnknownHostException {
        String productId = UUID.randomUUID()+"_"+ InetAddress.getLocalHost().getHostName()+"_"+new Date().getTime();
        CreateProductCommand createProductCommand =
                CreateProductCommand.builder()
                        .productId(productId)
                        .model(product.getModel())
                        .name(product.getName())
                        .price(product.getPrice())
                        .build();
       // String result = commandGateway.sendAndWait(createProductCommand);
         commandGateway.send(createProductCommand); //Use this - Fire and forget mechanism
        createProductCommandObjectCommandCallback = (commandMessage, commandResultMessage) -> {
            try {
                Thread.sleep(3000); // This will slow response and will wait until the Callback is Completed - So do not use it
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            log.info("{} **************SOMETHING CAME BACK ********NOT TO USE JUST EXPERIMENT******************* {} -- {}", commandMessage, commandResultMessage);
        };
        log.info("CONTROLLER GET CALLED ********************** 1");
        commandGateway.send(createProductCommand, createProductCommandObjectCommandCallback);

       // return "Product added : "+result;
        return "Product added :  "+productId;
    }

}
